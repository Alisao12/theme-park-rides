package com.wake.themeparkrides.repositories;

import com.wake.themeparkrides.model.ThemeParkRide;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ThemeParkRideRepository extends CrudRepository<ThemeParkRide, Long> {
    List<ThemeParkRide> findByName(String name);
}
